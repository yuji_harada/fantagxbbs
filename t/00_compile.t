use strict;
use warnings;
use Test::More;


use FantaGX;
use FantaGX::Web;
use FantaGX::Web::View;
use FantaGX::Web::ViewFunctions;

use FantaGX::DB::Schema;
use FantaGX::Web::Dispatcher;


pass "All modules can load.";

done_testing;
