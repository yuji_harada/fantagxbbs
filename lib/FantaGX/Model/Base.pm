package FantaGX::Model::Base;
use strict;
use warnings;
use utf8;
use File::Spec;

use FantaGX::DB;
use FantaGX::DB::Schema;

our $DB;

sub import {
    my $caller = caller;
    return unless $caller =~ /FantaGX::Model::/;

    no strict 'refs';
    *{"$caller\::db"} = \&db;
}

sub db {
    if ( !$DB ) {
        my $config_path = File::Spec->catfile( 'config', lc( $ENV{PLACK_ENV} || 'development' ) . '.pl' );
        my $config = do $config_path or die "config_path : $!";
        my $schema = FantaGX::DB::Schema->instance;
        $DB = FantaGX::DB->new(
            schema => $schema,
            connect_info => [ @{ $config->{DBI} } ],
            on_connect_do => [ 'SET SESSION sql_mode=STRICT_TRANS_TABLES;', ],
        );
    }
    return $DB;
}

1;
