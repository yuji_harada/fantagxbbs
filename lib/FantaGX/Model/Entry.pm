package FantaGX::Model::Entry;
use strict;
use warnings;
use utf8;

use FantaGX::Model::Base;

sub all {
    my ($class, $c) = @_;
    my @entries = db->search('entry');

    return \@entries;
}

1;
