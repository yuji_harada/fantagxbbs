package FantaGX::Web::C::Root;
use strict;
use warnings;
use utf8;

use FantaGX::Model::Entry;

sub index {
    my ($class, $c) = @_;
    my $entries = FantaGX::Model::Entry->all;

    return $c->render('index.tx', {entries => $entries});
}

sub create {
    my ($class, $c) = @_;
    my $body = $c->req->param('body');

    $c->db->insert('entry' => {body => $body} ) if $body;

    return $c->redirect('/');
}

1;
