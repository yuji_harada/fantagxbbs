package FantaGX::Web::Dispatcher;
use strict;
use warnings;
use utf8;
use Amon2::Web::Dispatcher::RouterBoom;

use FantaGX::Web::C::Root;
base "FantaGX::Web::C";

get  '/'             => 'Root#index';
post '/entry/create' => 'Root#create';

1;
