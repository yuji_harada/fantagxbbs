package FantaGX::DB::Schema;
use strict;
use warnings;
use utf8;

use Teng::Schema::Declare;

base_row_class 'FantaGX::DB::Row';

table {
    name 'session';
    pk 'id';
    columns qw(session_data);
};

table {
    name 'entry';
    pk 'id';
    columns qw(body);
};

1;
